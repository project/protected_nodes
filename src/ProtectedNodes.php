<?php

namespace Drupal\protected_nodes;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\EntityInterface;

/**
 * Service description.
 */
class ProtectedNodes {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a ProtectedNodes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    EntityFieldManagerInterface $entity_field_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * Gets all available node types.
   *
   * @return array
   *   An array of node types in format machine_name => label.
   */
  public function getAvailableNodeTypes(): array {
    $types = [];
    $loaded_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    array_walk($loaded_types, function ($type) use (&$types) {
      $types[$type->id()] = $type->label();
    });
    return $types;
  }

  /**
   * Ensures the protected field exists on a node type.
   *
   * @param string $node_type_name
   *   The node type name.
   */
  public function addFieldSupport($node_type_name) {
    $node_type = $this->entityTypeManager->getStorage('node_type')->load($node_type_name);
    if (empty($node_type)) {
      return;
    }

    $field_storage = FieldStorageConfig::loadByName('node', 'protected');
    if (empty($field_storage)) {
      /** @var \Drupal\field\Entity\FieldStorageConfig $field_storage */
      FieldStorageConfig::create([
        'field_name' => 'protected',
        'type' => 'boolean',
        'entity_type' => 'node',
        'cardinality' => 1,
      ])->save();
    }

    $field_storage = FieldStorageConfig::loadByName('node', 'protected');
    $field = FieldConfig::loadByName('node', $node_type->id(), 'protected');
    if (empty($field)) {
      $field = FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => $node_type->id(),
        'label' => 'Protected',
        'description' => 'Protects this node from being accidentally deleted.',
      ]);
      $field->save();

      // Assign widget settings for the default form mode.
      $this->entityDisplayRepository->getFormDisplay('node', $node_type->id())
        ->setComponent('protected', [
          'type' => 'boolean_checkbox',
          'region' => 'content',
        ])
        ->save();
    }
  }

  /**
   * Ensures the protected field does not exist on a node type.
   *
   * @param string $node_type_name
   *   The node type name.
   */
  public function removeFieldSupport($node_type_name) {
    $node_type = $this->entityTypeManager->getStorage('node_type')->load($node_type_name);
    if (empty($node_type)) {
      return;
    }

    // Delete the storage.
    $field_storage = FieldStorageConfig::loadByName('node', 'protected');
    if (!empty($field_storage)) {
      $field_storage->delete();
    }

    // Delete the field.
    $field = FieldConfig::loadByName('node', $node_type->id(), 'protected');
    if (!empty($field)) {
      $field->delete();
    }

    // Delete the widget.
    $component = $this->entityDisplayRepository->getFormDisplay('node', $node_type->id())
      ->getComponent('protected');
    if (!empty($component)) {
      $component->delete();
    }
  }

  /**
   * Checks if a given entity is protected against accidental deletion.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   Check result.
   */
  public function isProtected(EntityInterface $entity): bool {
    return ($entity->hasField('protected')
      && $entity->get('protected')->value);
  }

}
