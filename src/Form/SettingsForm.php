<?php

namespace Drupal\protected_nodes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Protected Nodes settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Protected nodes service.
   *
   * @var \Drupal\protected_nodes\ProtectedNodes
   */
  protected $protectedNodes;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'protected_nodes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['protected_nodes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->protectedNodes = $container->get('protected_nodes');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $all_node_types = $this->protectedNodes->getAvailableNodeTypes();
    $form['info1'] = [
      '#markup' => $this->t('<p>For the enabled node types, a checkbox field will be added to the node form to prevent them from being accidentally deleted.</p>'),
    ];
    $form['info2'] = [
      '#markup' => $this->t('<p>You will then need to edit the corresponding node forms and place that field widget in the form.</p>'),
    ];
    $form['supported_node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Supported node types'),
      '#description' => $this->t('Select the node types that should be supported by Protected Nodes.'),
      '#options' => $all_node_types,
      '#default_value' => $this->config('protected_nodes.settings')->get('supported_node_types') ?: [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Apply the set field changes.
    $original_value = $this->config('protected_nodes.settings')->get('supported_node_types') ?: [];
    $new_value = $form_state->getValue('supported_node_types');
    foreach ($new_value as $key => $value) {

      // Add support.
      if ($value && !in_array($key, $original_value)) {
        $this->protectedNodes->addFieldSupport($key);
      }

      // Remove support.
      elseif (!$value && in_array($key, $original_value)) {
        $this->protectedNodes->removeFieldSupport($key);
      }
    }

    // Save the configuration.
    $this->config('protected_nodes.settings')
      ->set('supported_node_types', $new_value)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
