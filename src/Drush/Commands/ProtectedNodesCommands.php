<?php

namespace Drupal\protected_nodes\Drush\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush commands for protected nodes module..
 */
class ProtectedNodesCommands extends DrushCommands {

  /**
   * Manually ensure the fields exist.
   *
   * This command needs to be run when the support has been added or removed
   * from a node type in protected_nodes_supported_types().
   *
   * @usage protected_nodes-ensureFields
   *   Usage description
   *
   * @command protected_nodes:ensureFields
   * @aliases prn_ef
   */
  public function ensureFields() {
    protected_nodes_ensure_fields();
  }

}
