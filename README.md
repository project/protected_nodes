# Protected Nodes

Adds a "Protected" field to the selected node types, and prevents accidental
deletion for non-admins.


## Installation

Install the module as usual,
see https://www.drupal.org/docs/7/extend/installing-modules for further
information.


## Usage

After the module has been installed, visit the settings page and configure which
node types will be eligible for protection. *See Configuration > System >
Protected Nodes*, or `/admin/config/system/settings/protected-nodes`

For the enabled node types, a new field, "Protected" gets added. You can check
the node types display mode to place the field where you want it in the node 
form.

When placed in the form, the node can be marked as "protected". For such nodes,
the "Delete" action is replaced with the warning text, and direct deleting of
the node id prohibited. Only unsetting the "protected" field will allow the node
to be deleted. Only admins and users with the "manage node protection" 
permission can unset the node protection field.

## Permissions

The module has 2 permissions - to administer the protected nodes, and to
configure the protected nodes module. 
- **Use Protected Nodes** means marking or un-marking a node 
as protected.
- **Configure Protected Nodes** means having access to the 
configuration form which configures, which content types will support protection.

## Security Disclaimer

This functionality is not meant to secure nodes from hackers. Evidently, there
are ways to unset the protection in database, in code, or remove the field via
field config ui. The intended use of this module is to prevent accidental
deletion by the editors.
